﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Microsoft.AspNet.SignalR;

namespace RadControlsWebApp1
{
    public class NotificationHub : Microsoft.AspNet.SignalR.Hub
    {

        public void SendNotification(string message)
        {
            this.Clients.All.Notify(message);
        }

    }
}