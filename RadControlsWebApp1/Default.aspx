﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<telerik:RadStyleSheetManager id="RadStyleSheetManager1" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
	<telerik:RadScriptManager ID="RadScriptManager1" runat="server">
		<Scripts>
			<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
			<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
			<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
		</Scripts>
	</telerik:RadScriptManager>
	<script type="text/javascript">
		//Put your JavaScript code here.
    </script>
	<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
	</telerik:RadAjaxManager>
	<div>
        Message to send to all connected clients:
        <input type="text" id="txtMessageToTransmit" />
        <br />
        <button id="submit">Transmit</button>


        <telerik:RadNotification runat="server" ID="radNotify"
            EnableShadow="True" EnableViewState="False" ShowCloseButton="False" 
            Title="Incoming Notification:" Animation="Slide" Width="300" Height="100" 
            OffsetX="-20" OffsetY="-20" Skin="Black">
        </telerik:RadNotification>

	</div>
	</form>

    <script src="Scripts/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.signalR-1.0.0-rc2.min.js" type="text/javascript"></script>
    <script src="/SignalR/hubs" type="text/javascript"></script>
    <script type="text/javascript"><!--

    $(function () {

        var notificationHub = $.connection.notificationHub;

        notificationHub.client.Notify = function(msg) {
            $(".notificationText").html(msg);
            var $radNotify = $find("<%= radNotify.ClientID%>");
            $radNotify.set_text(msg);
            $radNotify.show();
        };

        $.connection.hub.start();

        $("#submit").click(function (e) {
            e.preventDefault();
            var server = $.connection.notificationHub.server;
            server.sendNotification($("#txtMessageToTransmit").val());
            return false;
        });

    });

    //--></script>

</body>
</html>
